<?php

namespace App\Models;

use Conner\Likeable\Likeable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory, Likeable;

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['user', 'comments'];

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'content', 'user_id'
    ];

    /**
     * Get the user that owns the post.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the comments for the post.
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }

    /**
     * Set the automatic delete for the post.
     */
    protected static function boot(): void {
        parent::boot();

        self::deleting(function($post) {
            $post->comments()->each(function($comment) {
                $comment->content = "POST DELETED! I DON'T KNOW WHY I CAN'T DELETE RELATIONS";
                $comment->save(); // <-- executed immediately after the deletion

                $comment->delete(); // <-- not executed immediately after the deletion
            });
       });
    }
}
