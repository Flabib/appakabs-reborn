<?php

use Illuminate\Support\Str;

if (!function_exists('strPlural')) {
    function strPlural($length, $str) {
        return $length . " " . Str::plural($str, $length == 0 ? 1 : $length);
    }
}
