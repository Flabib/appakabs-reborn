<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Comment as CommentModel;

class Reply extends Component
{
    public $reply;

    public function mount($reply)
    {
        $this->reply = $reply;
    }

    public function render()
    {
        return view('livewire.reply');
    }

    public function like()
    {
        if ($this->reply->liked()) $this->reply->unlike();
        else $this->reply->like();

        $this->reply = CommentModel::find($this->reply->id);
    }
}
