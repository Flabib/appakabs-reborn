<?php

namespace App\Http\Livewire;

use App\Models\Comment;
use Livewire\Component;
use App\Models\Post as PostModel;

class Replies extends Component
{
    public $replies, $commentId, $postId, $content;

    protected $rules = [
        'content' => 'required',
    ];

    public function mount($comment)
    {
        $this->commentId = $comment->id;
        $this->postId = $comment->commentable_id;
    }

    public function render()
    {
        $this->replies = Comment::find($this->commentId)->replies()->oldest()->get();

        return view('livewire.replies');
    }

    public function store()
    {
        $this->validate();

        $reply = new Comment;
        $reply->content = $this->content;
        $reply->user()->associate(auth()->user());
        $reply->parent_id = $this->commentId;

        $post = PostModel::find($this->postId);
        $post->comments()->save($reply);

        $this->resetInput();
        $this->emit('commentAdded');
    }

    private function resetInput()
    {
        $this->content = null;
    }
}
