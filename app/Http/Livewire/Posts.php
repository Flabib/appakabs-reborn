<?php

namespace App\Http\Livewire;

use App\Models\Post;
use Livewire\Component;

class Posts extends Component
{
    public $content, $amount = 5;

    protected $rules = [
        'content' => 'required',
    ];

    public function render()
    {
        $posts = Post::latest()->take($this->amount)->get();
        $total = Post::count();

        return view('livewire.posts', compact('posts', 'total'));
    }

    public function store()
    {
        $this->validate();

        Post::create([
            'content' => $this->content,
            'user_id' => auth()->id(),
        ]);

        $this->resetInput();
    }

    public function loadMore()
    {
        $this->amount += 5;
    }

    private function resetInput()
    {
        $this->content = null;
    }
}
