<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Comment as CommentModel;

class Comment extends Component
{
    public $comment, $isDetail;

    public function mount($comment, $isDetail = false)
    {
        $this->comment = $comment;
        $this->isDetail = $isDetail;
    }

    public function render()
    {
        return view('livewire.comment');
    }

    public function like()
    {
        if ($this->comment->liked()) $this->comment->unlike();
        else $this->comment->like();

        $this->comment = CommentModel::find($this->comment->id);
    }

    public function show() {
        return redirect()->route('comment.show', $this->comment);
    }
}
