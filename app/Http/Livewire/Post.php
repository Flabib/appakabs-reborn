<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Post as PostModel;

class Post extends Component
{
    public $post, $isDetail;

    protected $listeners = ['commentAdded' => '$refresh'];

    public function mount($post, $isDetail = false)
    {
        $this->post = $post;
        $this->isDetail = $isDetail;
    }

    public function render()
    {
        return view('livewire.post');
    }

    public function like()
    {
        if ($this->post->liked()) $this->post->unlike();
        else $this->post->like();

        $this->post = PostModel::find($this->post->id);
    }

    public function show() {
        return redirect()->route('post.show', $this->post);
    }
}
