<?php

namespace App\Http\Livewire;

use App\Models\Comment;
use Livewire\Component;
use App\Models\Post as PostModel;

class Comments extends Component
{
    public $comments, $postId, $content;

    protected $rules = [
        'content' => 'required',
    ];

    public function mount($post)
    {
        $this->postId = $post->id;
    }

    public function render()
    {
        $this->comments = PostModel::find($this->postId)->comments()->oldest()->get();

        return view('livewire.comments');
    }

    public function store()
    {
        $this->validate();

        $comment = new Comment;
        $comment->content = $this->content;
        $comment->user()->associate(auth()->user());

        $post = PostModel::find($this->postId);
        $post->comments()->save($comment);

        $this->resetInput();
        $this->emit('commentAdded');
    }

    private function resetInput()
    {
        $this->content = null;
    }
}
