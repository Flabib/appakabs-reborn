<?php

namespace App\Http\Controllers;

use App\Models\Comment;

class CommentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a replies of the comment.
     */
    public function show(Comment $comment)
    {
        return view('comment.show', compact('comment'));
    }
}
