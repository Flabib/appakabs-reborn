<div class="card mt-3">
    <div class="card-body">
        @forelse ($comments as $comment)
            <livewire:comment :comment="$comment" :key="$comment->id" />
        @empty
            <p class="text-center">No comments yet</p>
        @endforelse

        <input autocomplete="off" name="content" type="text" class="form-control @error('content') is-invalid @enderror" id="content" wire:model="content" />
        <button type="button" class="btn btn-primary float-right mt-2" wire:click="store">Comment</button>
    </div>
</div>
