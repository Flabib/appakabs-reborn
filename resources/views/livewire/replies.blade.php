<div class="card mt-3">
    <div class="card-body">
        @forelse ($replies as $reply)
            <livewire:reply :reply="$reply" :key="$reply->id" />
        @empty
            <p class="text-center">No replies yet</p>
        @endforelse

        <input autocomplete="off" name="content" type="text" class="form-control @error('content') is-invalid @enderror" id="content" wire:model="content" />
        <button type="button" class="btn btn-primary float-right mt-2" wire:click="store">Reply</button>
    </div>
</div>
