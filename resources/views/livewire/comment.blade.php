<div>
    <div class="d-flex justify-content-between">
        <h6><b><a href="#">{{ $comment->user->name }}</a></b></h6>
        <small>{{ $comment->created_at->diffForHumans() }}</small>
    </div>
    <p class="mb-2">{{ $comment->content }}</p>
    <div class="d-flex justify-content-between align-items-center">
        <h6 class="mb-0">{{ strPlural($comment->likeCount, 'like') }} {{ strPlural($comment->replies->count(), 'reply') }}</h6>
        <div>
            <button type="button" class="btn btn-light btn-sm mx-1 {{ $comment->liked() ? 'text-primary' : 'text-dark' }}" wire:click="like" onclick="this.blur();">
                <i class="fa fa-thumbs-up"></i> Like
            </button>
            @unless ($isDetail)
            <button type="button" class="btn btn-light btn-sm mx-1" wire:click="show" onclick="this.blur();">
                <i class="fa fa-reply"></i> Reply
            </button>
            @endunless
        </div>
    </div>
    @unless ($isDetail) <hr /> @endunless
</div>
