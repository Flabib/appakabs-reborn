<div>
    <div class="card mt-3">
        <div class="card-header">
            What do you think?
        </div>

        <div class="card-body">
            <form>
                <textarea name="content" class="form-control @error('content') is-invalid @enderror" id="content" cols="30" rows="5" wire:model="content" autocomplete="off"></textarea>
                @error('content') <small class="text-danger">{{ $message }}</small> @enderror
                <button type="button" class="btn btn-primary mt-3 float-right" wire:click="store">Post</button>
            </form>
        </div>
    </div>

    @foreach ($posts as $post)
        <livewire:post :post="$post" :key="$post->id" />
    @endforeach

    @if ($total > $amount)
        <div class="d-flex justify-content-center mt-3">
            <button class="btn btn-light" wire:click="loadMore" onclick="this.blur();">Load More</button>
        </div>
    @endif
</div>
