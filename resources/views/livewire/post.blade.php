<div class="card mt-3">
    <div class="card-body">
        <div class="d-flex justify-content-between">
            <h5 class="card-title"><b><a href="#">{{ $post->user->name }}</a></b></h5>
            <small>{{ $post->created_at->diffForHumans() }}</small>
        </div>
        <p class="card-text">{{ $post->content }}</p>
    </div>
    <div class="card-footer">
        <div class="d-flex justify-content-between">
            <p class="mb-0">{{ strPlural($post->likeCount, 'like') }} {{ strPlural($post->comments->count(), 'comment') }}</p>
            <div class="d-flex justify-content-center align-items-center">
                <button type="button" class="btn btn-light btn-sm mx-1 {{ $post->liked() ? 'text-primary' : 'text-dark' }}" wire:click="like" onclick="this.blur();">
                    <i class="fa fa-thumbs-up"></i> Like
                </button>
                @unless ($isDetail)
                <button type="button" class="btn btn-light btn-sm mx-1" wire:click="show" onclick="this.blur();">
                    <i class="fa fa-comment"></i> Comment
                </a>
                @endunless
            </div>
        </div>
    </div>
</div>
