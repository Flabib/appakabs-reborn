<div>
    <div class="d-flex justify-content-between">
        <h6><b><a href="#">{{ $reply->user->name }}</a></b></h6>
        <small>{{ $reply->created_at->diffForHumans() }}</small>
    </div>
    <p class="mb-2">{{ $reply->content }}</p>
    <div class="d-flex justify-content-between align-items-center">
        <h6 class="mb-0">{{ strPlural($reply->likeCount, 'like') }}</h6>
        <div>
            <button type="button" class="btn btn-light btn-sm mx-1 {{ $reply->liked() ? 'text-primary' : 'text-dark' }}" wire:click="like" onclick="this.blur();">
                <i class="fa fa-thumbs-up"></i> Like
            </button>
        </div>
    </div>
    <hr />
</div>
